const express = require("express");
const { register, verifyEmail, login } = require("../controllers/user");

const router = express.Router();

// user routes
router.post("/register", register);
router.post("/verify-email", verifyEmail);
router.post("/login", login);

module.exports = router;
