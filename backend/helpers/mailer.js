const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const { OAuth2 } = google.auth;

exports.sendVerificationEmail = (email, name, url) => {
  const { EMAIL, MAILING_REFRESH_TOKEN, MAILING_ID, MAILING_SECRET } =
    process.env;
  const oauth_link = "https://developers.google.com/oauthplayground";

  const auth = new OAuth2(
    MAILING_ID,
    MAILING_SECRET,
    MAILING_REFRESH_TOKEN,
    oauth_link
  );

  auth.setCredentials({ refresh_token: MAILING_REFRESH_TOKEN });

  // console.log("MAILING_ID is ", MAILING_ID);
  // console.log("MAILING_SECRET is ", MAILING_SECRET);
  // console.log("MAILING_REFRESH_TOKEN is ", MAILING_REFRESH_TOKEN);
  // console.log("oauth_link is ", oauth_link);
  // console.log("EMAIL is ", EMAIL);

  const accessToken = auth.getAccessToken();

  const stmp = nodemailer.createTransport({
    service: "gmail",
    auth: {
      type: "OAuth2",
      user: EMAIL,
      clientId: MAILING_ID,
      clientSecret: MAILING_SECRET,
      refreshToken: MAILING_REFRESH_TOKEN,
      accessToken,
    },
  });
  const mailOptions = {
    from: `REACTION BACKBENCHERS <${EMAIL}>`,
    to: email,
    subject: "REACTION BACKBENCHERS - Verify your email",
    html: `<divstyle="max-width: 700px;margin: auto;height: auto;border: 10px solid #ddd;padding: 50px 20px;font-size: 110%;"><h2 style="text-align: center;text-transform: uppercase;color: teal">Hello ${name},</h2><h3 style="text-align: center;text-transform: uppercase;color: teal">Welcome to REACTION BACKBENCHERS.</h3><p>Congratulations! You're almost set to start using REACTION BACKBENCHERS.Just click the button below to validate your email address.</p><a href="${url} target="_blank" "style="background: crimson;text-decoration: none;color: white;padding: 10px 20px;margin: 10px 0;display: inline-block;">Verify your email address</a><p>If the button doesn't work for any reason, you can also click on thelink below:</p><pstyle="overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;hyphens: auto;"><a href="${url}" target="_blank">${url}</a></p></div>`,
  };

  stmp.sendMail(mailOptions, (err, res) => {
    if (err) return err?.message;
    return res;
  });
};
