const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const userRoutes = require("./routes/user");

const app = express();
dotenv.config();
app.use(cors());
app.use(express.json());

// middlewares

//routes
app.use("/api", userRoutes);

//database
mongoose
  .connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
  })
  .then(() => console.log("Database connected successfully"))
  .catch((err) => console.log("database error is ,", err));

//server
const PORT = process.env.PORT || 8000;
app.listen(PORT, () => console.log("server is listening on port ", PORT));
