const { sendVerificationEmail } = require("../helpers/mailer");
const { generateToken } = require("../helpers/tokens");
const {
  validateEmail,
  validateLength,
  validateUsername,
} = require("../helpers/validation");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

exports.register = async (req, res) => {
  try {
    const {
      first_name,
      last_name,
      email,
      username,
      bYear,
      bMonth,
      bDay,
      gender,
      password,
    } = req.body;

    if (!validateEmail(email)) {
      return res.status(400).json({ message: "invalid email address" });
    }

    const check = await User.findOne({ email });
    if (check) {
      return res.status(400).json({ message: "email already exists" });
    }

    if (!validateLength(first_name, 1, 30)) {
      return res
        .status(400)
        .json({ message: "first_name must be between 1 and 30 characters" });
    }

    if (!validateLength(last_name, 1, 30)) {
      return res
        .status(400)
        .json({ message: "last_name must be between 1 and 30 characters" });
    }

    if (!validateLength(password, 6, 12)) {
      return res
        .status(400)
        .json({ message: "password must be between 6 and 12 characters" });
    }

    let tempUsername = first_name + last_name;
    let newUsername = await validateUsername(tempUsername);

    const cryptedPassword = await bcrypt.hash(password, 12);
    // console.log(cryptedPassword);

    // return;

    const user = await new User({
      first_name,
      last_name,
      email,
      username: newUsername,
      bYear,
      bMonth,
      bDay,
      gender,
      password: cryptedPassword,
    }).save();

    const emailVerificationToken = generateToken(
      { id: user._id.toString() },
      "30m"
    );

    // console.log(emailVerificationToken);

    const url = `${process.env.BASE_URL}/verify-email/${emailVerificationToken}`;

    // console.log("url is ", url);

    sendVerificationEmail(user.email, user.first_name, url);

    const token = generateToken({ id: user._id.toString() }, "7d");
    res.send({
      id: user._id,
      first_name: user.first_name,
      last_name: user.last_name,
      verified: user.verified,
      picture: user.picture,
      username: user.username,
      message:
        "Registration successful! Please check your email for verification.",
      token: token,
    });
  } catch (error) {
    res.status(500).json({ message: error?.message });
  }
};

exports.verifyEmail = async (req, res) => {
  try {
    const { token } = req.body;

    const user = jwt.verify(token, process.env.SECRET_TOKEN_KEY);

    const check = await User.findById(user.id);

    // console.log("check is ", check.verified);

    if (check.verified == true) {
      return res
        .status(400)
        .json({ message: "this email is already verified" });
    } else {
      await User.findByIdAndUpdate(user.id, { verified: true });
      // console.log("in else", user);
      return res.status(200).json({ message: "email verified successfully" });
    }

    // console.log("token is ", user);
  } catch (error) {
    res.status(500).json({ message: error?.message });
  }
};

exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email });
    if (!user) {
      return res.status(400).json({ message: "email does not exist" });
    }

    const check = await bcrypt.compare(password, user.password);
    if (!check) {
      return res.status(400).json({ message: "password is incorrect" });
    }

    const token = generateToken({ id: user._id.toString() }, "7d");
    res.send({
      id: user._id,
      first_name: user.first_name,
      last_name: user.last_name,
      verified: user.verified,
      picture: user.picture,
      username: user.username,
      message: "Login successful!!",
      token: token,
    });
  } catch (error) {
    res.status(500).json({ message: error?.message });
  }
};
